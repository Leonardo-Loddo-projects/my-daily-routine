<?php

namespace App\Http\Controllers;

use Exception;
use App\Models\Article;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\ArticleRequest;
use App\Http\Requests\ArticleUpdateRequest;

class ArticleController extends Controller
{
    public function __construct(){
        $this -> middleware('auth')->except('index, detail');
    }
    public function create(){
        return view('article.create');
    } 
    public function store(ArticleRequest $request){
        try{
            $article = Article::create([
                'title' => $request->title,
                'cover' => $request->file('cover')->store('public/covers'),
                'description' => $request->description,
                'user_id' => Auth::user()->id,
             ]);
         }catch(Exception $e){
             return redirect(route('homePage'))->with('articleFail', 'Non è stato possibile pubblicare il tuo Articolo, riprova più tardi!');
         };
        return redirect(route('homePage'))->with('articleCreated', 'Hai postato con successo il tuo Articolo !');
    }
    public function index(){
        $articles = Article::all();
        return view('article.index', compact('articles'));
    }
    public function detail($id){
        $articles = Article::all();
        foreach ($articles as $article) {
            if ($article['id'] == $id) {
                return view('article.detail',['article'=>$article]);
            }
        }    
    }
    public function edit(Article $article)
    {
        return view(('article.edit'), compact('article')); 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Author  $author
     * @return \Illuminate\Http\Response
     */
    public function update(ArticleUpdateRequest $request, Article $author)
    {
        if($request->cover){
            $author->update([
                'title' => $request->title,
                'cover' => $request->file('cover')->store('public/covers'),
                'description' => $request->description,
            ]);
        }else{
            $author->update([
                'title' => $request->title,
                'description' => $request->description,
            ]);
        }
        return redirect(route('article.index'))->with('articleEdit', 'Hai modificato la pagina con successo !' );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Author  $author
     * @return \Illuminate\Http\Response
     */
    public function destroy(Article $article)
    {
        $article->delete();
        return redirect(route('article.index'))->with('articleEdit', 'Hai eliminato la pagina con successo !' );
    }
}


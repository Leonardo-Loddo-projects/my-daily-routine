<?php

namespace App\Http\Controllers;


use Exception;
use App\Models\Author;
use App\Models\Article;
use App\Mail\ContactMail;
use Illuminate\Http\Request;
use PhpParser\Node\Stmt\TryCatch;
use App\Http\Requests\MailRequest;
use Illuminate\Support\Facades\Mail;

class PublicController extends Controller
{
    public function homePage(){
        $articles = Article::orderBy('created_at', 'DESC')->take(3)->get();
        $authors = Author::orderBy('created_at', 'DESC')->take(3)->get();
        return view('welcome', compact('articles'), compact('authors'));
    }
    public function profile(){
        return view('profile');
    }
    public function submit(MailRequest $request){
        $email = $request->input('email');
        $name = $request->input('name');
        $message = $request->input('message');
        $userData = compact('name', 'message');
        try{
            Mail::to($email)->send(new ContactMail($userData));
        }catch(Exception $e){
            return redirect(route('homePage'))->with('fail', 'Non è stato possibile inviare il tuo messaggio, riprova più tardi!');
        };
        return redirect(route('homePage'))->with('success', 'Il tuo messaggio é stato inviato con successo!');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Author extends Model
{
    use HasFactory;
    //ATTRIBUTI
    protected $fillable = [
        'name',
        'email',
        'phone',
        'address',
        'photo',
        'bio',
    ];
}

<x-layout>
    <x-slot name="title">Modifica Autore</x-slot>
    <div class="container my-1 my-md5 content-center">
        <h1 class="fw-bold text-center">Modifica le informazione dell'Autore</h1>
        {{-- mostra errori inserimento --}}
        @if($errors->any())
          <div class="alert alert-danger">
            <ul>
              @foreach ($errors->all() as $error)
                <li class="text-danger">{{ $error }}</li>
              @endforeach
            </ul>
          </div>          
        @endif
        <div class="container my-4">
           <div class="col-12 md-8">
              <form class="p-5 rounded text-bg-dark" method="POST" action="{{route('author.update', compact('author'))}}" enctype="multipart/form-data">
                @csrf
                @method('put')
                <h5>Modifica un Autore</h5>
                <p class="mb-3">Modifica i campi con le informazioni dell'Autore</p>
                <div class="mb-3">
                  <label for="name" class="form-label">Nome</label>
                  <input type="text" name="name" class="form-control" id="name" value="{{$author['name']}}">
                </div>
                <div class="mb-3">
                  <label for="email" class="form-label">Email</label>
                  <input type="email" name="email" class="form-control" id="email" value="{{$author['email']}}">  
                </div>
                <div class="mb-3">
                    <label for="phone" class="form-label">Numero di Telefono</label>
                    <input type="text" name="phone" class="form-control" id="phone" value="{{$author['phone']}}">  
                </div>
                <div class="mb-3">
                    <label for="address" class="form-label">Indirizzo</label>
                    <input type="text" name="address" class="form-control" id="address" value="{{$author['address']}}">  
                </div>
                <div class="mb-3">
                    <img src="{{Storage::url($author->photo)}}" class="img rounded" alt="attuale foto">
                </div>
                <div class="mb-3">
                    <label for="photo" class="form-label">Foto Autore</label>
                    <input type="file" name="photo" class="form-control" id="photo" >  
                </div>
                <div class="mb-3">
                  <label for="bio" class="form-label">Breve Biografia</label>
                  <textarea rows="7" name="bio" class="form-control" id="bio">{{$author['bio']}}</textarea>
                </div>
                <button type="submit" class="btn btn-success">Crea Autore</button>
                <a href="{{route('author.index')}}" class="btn btn-info text-light">Torna Indietro</a>
              </form>
              <form class="mt-5 " method="POST" action="{{route('author.destroy', compact('author'))}}">
                @csrf
                @method('delete')
                <button type="submit" class="btn d-inline btn-danger">Cancella Autore</button>
              </form>
           </div>
        </div>
    </div>
</x-layout>
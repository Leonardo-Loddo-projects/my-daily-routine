<x-layout>
    <x-slot name="title">{{$author['name']}}</x-slot>
    <div class="container my-5">
        <h1 class="fw-bold text-center">{{$author['name']}}</h1>
        <div class="row justify-content-center m-5">
            <div class="order-2 order-md-1 col-12 col-md-6 bg-info text-light p-5 rounded">
                <p>{{$author['bio']}}</p>
            </div>
            <div class="order-1 order-md-2 col-12 col-md-6 mb-5 mb-md-0">
                <img src="{{Storage::url($author->photo)}}" class="img card-img-top rounded" alt="copertina">
            </div>
        </div>
        <h3 class="fw-bold text-center">Contatta {{$author['name']}}</h3>
        <div class="row justify-content-center m-5">
            <p>Email: {{$author['email']}} Telefono: {{$author['phone']}} Indirizzo: {{$author['address']}} <a href="{{route('author.edit',compact('author'))}}" class="btn btn-info text-light">Modifica Pagina</a></p>
        </div>
    </div>
</x-layout>
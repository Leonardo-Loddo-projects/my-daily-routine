<x-layout>
    {{-- DEFINISCO IL TITOLO --}}
    <x-slot name="title">Articoli</x-slot>
    @if(session('authorEdit'))
        <div class="alert alert-success m-1">
            <p class="m-1">{{session('authorEdit')}}</p>
        </div>
    @endif
    <div class="container my-5">
        <h6 class="fw-bold text-center">Autori</h6>
        <div class="row justify-content-center">
            @if (count($authors) > 0)
                @foreach($authors as $author)
                    <div class="col-12 col-md-4">
                        <div class="card mb-3">
                            <img src="{{Storage::url($author->photo)}}" class="img card-img-bottom" alt="copertina">
                            <div class="card-body">
                            <h5 class="card-title">{{$author['name']}}</h5>
                            <h6 class="card-subtitle mb-2 text-muted">{{$author['email']}}</h6>
                            <a href="{{route('author.show',compact('author'))}}" class="btn btn-info text-light">Pagina Autore</a>
                            </div>
                        </div>
                    </div>
                @endforeach
            @else
            <div class="col-12 col-md-5">
                <h2>Ancora nessun autore... <a href="{{route('author.create')}}" class="btn btn-success text-light">Aggiungine uno</a></h2>
            </div>    
            @endif
        </div>
    </div>
</x-layout>
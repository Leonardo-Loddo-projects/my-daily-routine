<x-layout>
    <x-slot name="title">Iscriviti</x-slot>
    <div class="container my-1 my-md5 content-center">
        <h1 class="fw-bold text-center">Iscriviti ora !</h1>
        {{-- mostra errori inserimento --}}
        @if($errors->any())
          <div class="alert alert-danger">
            <ul>
              @foreach ($errors->all() as $error)
                <li class="text-danger">{{ $error }}</li>
              @endforeach
            </ul>
          </div>          
        @endif
        <div class="container my-4">
           <div class="col-12 md-8">
              <form class="p-5 rounded text-bg-dark" method="POST" action="{{route('register')}}">
                @csrf
                <h5>Registrati</h5>
                <p class="mb-3">Crea il tuo MyDailyAccount</p>
                <div class="mb-3">
                  <label for="name" class="form-label">Username</label>
                  <input type="text" name="name" class="form-control" id="name" value="{{old('name')}}">
                </div>
                <div class="mb-3">
                  <label for="email" class="form-label">Email</label>
                  <input type="email" name="email" class="form-control" id="email" value="{{old('email')}}">  
                </div>
                <div class="mb-3">
                    <label for="password" class="form-label">Password</label>
                    <input type="password" name="password" class="form-control" id="password">  
                </div>
                <div class="mb-3">
                    <label for="password_confirmation" class="form-label">Conferma Password</label>
                    <input type="password" name="password_confirmation" class="form-control" id="password_confirmation">  
                </div>
                <button type="submit" class="btn btn-success">Iscriviti</button>
              </form>
           </div>
        </div>
    </div>
</x-layout>
<x-layout>
    <x-slot name="title">Profilo di {{Auth::user()->name}}</x-slot>
    {{-- articoli --}}
    <div class="container my-5">
        <h1 class="fw-bold text-center">Tutti i Post di {{Auth::user()->name}}</h1>
        <h3 class="fw-bold text-center">Solo tu puoi modificare o cancellare i tuoi post</h3>
        <div class="row justify-content-center">
            @if (count(Auth::user()->articles) > 0)
                @foreach(Auth::user()->articles as $article)
                    <div class="col-12 col-md-3">
                        <div class="card" style="width: 18rem;">
                            <img src="{{Storage::url($article->cover)}}" class="img card-img-top" alt="copertina">
                            <div class="card-body">
                            <h5 class="card-title">{{$article['title']}}</h5>
                            <h6 class="card-subtitle mb-2 text-muted">{{$article->user->name}}</h6>
                            <a href="{{route('article.detail', ['id' => $article['id']])}}" class="btn btn-info text-light">Leggi articolo</a>
                            </div>
                        </div>
                    </div>
                @endforeach
            @else
            <div class="col-12 col-md-5">
                <h2>Non hai ancora scritto nessun Articolo... <a href="{{route('article.create')}}" class="btn btn-success text-light">Scrivine uno</a></h2>
            </div>
            @endif
        </div>
    </div>
</x-layout>
<x-layout>
    <x-slot name="title">Articoli</x-slot>
    {{-- articoli --}}
    <div class="container my-5">
        <h1 class="fw-bold text-center">Tutti i Post</h1>
        @if(session('articleEdit'))
        <div class="alert alert-success m-1">
            <p class="m-1">{{session('articleEdit')}}</p>
        </div>
        @endif
        <div class="row justify-content-center">
            @if (count($articles) > 0)
                @foreach($articles as $article)
                    <div class="col-12 col-md-3">
                        <div class="card" style="width: 18rem;">
                            <img src="{{Storage::url($article->cover)}}" class="card-img-top img" alt="copertina">
                            <div class="card-body">
                            <h5 class="card-title">{{$article['title']}}</h5>
                            <h6 class="card-subtitle mb-2 text-muted">{{$article->user->name}}</h6>
                            <a href="{{route('article.detail', ['id' => $article['id']])}}" class="btn btn-info text-light">Leggi articolo</a>
                            </div>
                        </div>
                    </div>
                @endforeach
            @else
            <div class="col-12 col-md-5">
                <h2>Ancora nessun articolo... <a href="{{route('article.create')}}" class="btn btn-success text-light">Scrivine uno</a></h2>
            </div>    
            @endif
        </div>
    </div>
</x-layout>
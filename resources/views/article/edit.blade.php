<x-layout>
    <x-slot name="title">Modifica il tuo Post</x-slot>
    <div class="container my-1 my-md5 content-center">
        <h1 class="fw-bold text-center">Modifica subito un post!</h1>
        {{-- mostra errori inserimento --}}
        @if($errors->any())
          <div class="alert alert-danger">
            <ul>
              @foreach ($errors->all() as $error)
                <li class="text-danger">{{ $error }}</li>
              @endforeach
            </ul>
          </div>          
        @endif
        <div class="container my-4">
           <div class="col-12 md-8">
              <form class="p-5 rounded text-bg-dark" method="POST" action="{{route('article.update', compact('article'))}}" enctype="multipart/form-data">
                @csrf
                @method('PUT')
                

                <h5>Scrivi il tuo Post</h5>
                <p class="mb-3">Riempi i campi e condividi la tua Routine con la Community</p>
                <div class="mb-3">
                  <label for="title" class="form-label">Titolo</label>
                  <input type="text" name="title" class="form-control" id="title" value="{{$article ['title']}}">
                </div>
                <div class="mb-3">
                    <img src="{{Storage::url($article->cover)}}" class="img rounded" alt="attuale foto">
                </div>
                <div class="mb-3">
                    <label for="cover" class="form-label">Copertina</label>
                    <input type="file" name="cover" class="form-control" id="cover" >  
                </div>
                <div class="mb-3">
                  <label for="description" class="form-label">Descrizione</label>
                  <textarea rows="7" name="description" class="form-control" id="description">{{$article ['description']}}</textarea>
                </div>
                <button type="submit" class="btn btn-success">Salva Modifiche</button>
                <a href="{{route('article.index')}}" class="btn btn-info text-light">Torna Indietro</a>
              </form>
              <form class="mt-5" method="POST" action="{{route('article.destroy', compact('article'))}}">
                @csrf
                @method('delete')
                <button class="btn d-inline btn-danger">Cancella Articolo</button>
             </form>
           </div>
        </div>
    </div>
</x-layout>
<x-layout>
    <x-slot name="title">{{$article['title']}}</x-slot>
    <div class="container my-5">
        <h1 class="fw-bold text-center">{{$article['title']}}</h1>
        <h6 class="fw-bold text-center">Scritto da {{$article->user->name}}</h6>
        <div class="row justify-content-center m-5">
            <div class="order-2 order-md-1 col-12 col-md-6 bg-info text-light p-5 rounded">
                <p>{{$article['description']}}</p>
            </div>
            <div class="order-1 order-md-2 col-12 col-md-6 mb-5 mb-md-0">
                <img src="{{Storage::url($article->cover)}}" class="card-img-top img rounded" alt="copertina">
            </div>
        </div>
        {{-- se hai creato tu l'articolo , ti permette di modificarlo --}}
        @if(Auth::user() && Auth::user()->id == $article->user_id)
        <div class="row justify-content-center m-5">
            <div><a href="{{route('article.edit',compact('article'))}}" class="btn btn-info text-light">Modifica Articolo</a></div>
        </div>
        @endif
    </div>
</x-layout>
<!doctype html>
<html lang="it">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    {{-- IMPOSTA IL TITOLO DINAMICAMENTE --}}
    <title>{{$title ?? 'MyDailyRoutine.com'}}</title>
    {{-- COLLEGA I FOGLI DI STILE E I FILE JS --}}
    @vite(['resources/css/app.css', 'resources/js/app.js'])
  </head>
  <body class="">
    <x-navbar/>
        {{-- CONTENUTO DINAMICO --}}
        {{$slot}}
    <x-footer/>
  </body>
</html>
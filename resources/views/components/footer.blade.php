<div class="container-fluid w-100 bg-success">
    <footer class="py-5 ">
      <div class="row">
        <div class="col-6 col-md-3 mb-3 text-light">
          <h5>Indice</h5>
          <ul class="nav flex-column">
                <li class="nav-item mb-2"><a href="{{Route('homePage')}}" class="nav-link p-0 text-muted {{Route::is('homePage') ? 'active' : ''}}">Home</a></li>
                <li class="nav-item mb-2"><a href="{{Route('article.create')}}" class="nav-link p-0 text-muted {{Route::is('article.create') ? 'active' : ''}}">Crea un post</a></li>
                <li class="nav-item mb-2"><a href="{{Route('article.index')}}" class="nav-link p-0 text-muted {{Route::is('article.index') ? 'active' : ''}}">Articoli</a></li>
                <li class="nav-item mb-2"><a class="nav-link p-0 text-muted {{Route::is('author.create') ? 'active' : ''}}" aria-current="page" href="{{Route('author.create')}}">Registra un Autore</a></li>
                <li class="nav-item mb-2"><a class="nav-link p-0 text-muted {{Route::is('author.index') ? 'active' : ''}}" aria-current="page" href="{{Route('author.index')}}">Autori</a></li>
                @auth
                  <li class="nav-item mb-2"><a class="nav-link p-0 text-muted " aria-current="page" href="#" onclick="event.preventDefault(); document.querySelector('#form-logout').submit();">Logout</a></li>
                  <form action="{{route('logout')}}" id="form-logout" method="POST" class="d-none">
                    @csrf
                  </form>
                @else
                  <li class="nav-item mb-2"><a class="nav-link p-0 text-muted  {{Route::is('register') ? 'active' : ''}}" href="{{route('register')}}">Registrati</a></li>
                  <li class="nav-item mb-2"><a class="nav-link p-0 text-muted  {{Route::is('login') ? 'active' : ''}}" href="{{route('login')}}">Login</a></li>
                @endauth
          </ul>
        </div>
        <div class="col-6 col-md-3 mb-3 text-light">
            <h5>Contatti</h5>
            <ul class="nav flex-column">
                <li class="nav-item mb-2"><h6>Telefono:</h6><p>+39 333 351 26 73</p></li>
                <li class="nav-item mb-2"><h6>Email:</h6><p>20loddoloddo@gmail.com</p></li>
                <li class="nav-item mb-2"><h6>Instagram:</h6><a href="https://www.instagram.com/loddo.sf/" class="text-light p-0 ">@loddo.sf</a></li>
                <li class="nav-item mb-2"><h6>LinkedIn:</h6><a href="https://www.linkedin.com/in/leonardo-loddo-full-stack-web/" class="text-light p-0 ">Leonardo Loddo</a></li>
            </ul>
        </div>
        <div class="col-md-4 offset-md-1 mb-3 text-light">
          @if($errors->any())
          <div class="alert alert-danger">
            <ul>
              @foreach ($errors->all() as $error)
                <li class="text-danger">{{ $error }}</li>
              @endforeach
            </ul>
          </div>          
          @endif
            <form class="" method="POST" action="{{route('submit')}}">
                @csrf
                <h5>Contattaci</h5>
                <p class="mb-3">Mandaci un messaggio, ti contatteremo.</p>
                <div class="mb-3">
                  <label for="name" class="form-label">Nome e Cognome</label>
                  <input type="text" name="name" class="form-control" id="name" value="{{old('name')}}">
                </div>
                <div class="mb-3">
                  <label for="email" class="form-label">Email</label>
                  <input type="text" name="email" class="form-control" id="email" value="{{old('email')}}">  
                </div>
                <div class="mb-3">
                  <label for="message" class="form-label">Messaggio</label>
                  <textarea rows="7" name="message" class="form-control" id="message">{{old('message')}}</textarea>
                </div>
                <button type="submit" class="btn btn-success">Invia</button>
            </form>
        </div>
      </div>
      <div class="d-flex flex-column flex-sm-row justify-content-between py-4 my-4 border-top text-light">
        <p>© 2022 Company, Inc. All rights reserved.</p>
        <ul class="list-unstyled d-flex">
          <li class="ms-3"><a class="link-dark" href="#"><svg class="bi" width="24" height="24"><use xlink:href="#twitter"></use></svg></a></li>
          <li class="ms-3"><a class="link-dark" href="#"><svg class="bi" width="24" height="24"><use xlink:href="#instagram"></use></svg></a></li>
          <li class="ms-3"><a class="link-dark" href="#"><svg class="bi" width="24" height="24"><use xlink:href="#facebook"></use></svg></a></li>
        </ul>
      </div>
    </footer>
</div>
<nav class="navbar navbar-expand-lg navbar-dark bg-success">
    <div class="container-fluid ">
      <a class="navbar-brand" href="{{route('homePage')}}">MyDailyRoutine.com</a>
      <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
        <div class="navbar-nav">
          {{-- ELEMENTI NAVBAR  --}}
          {{-- A SECONDA DELLA ROUTE I LINK DIVENTANO ACTIVE DINAMICAMENTE --}}
          <a class="nav-link {{Route::is('homePage') ? 'active' : ''}}" aria-current="page" href="{{Route('homePage')}}">Home</a>
          <a class="nav-link {{Route::is('article.create') ? 'active' : ''}}" aria-current="page" href="{{Route('article.create')}}">Crea un Post</a>
          <a class="nav-link {{Route::is('article.index') ? 'active' : ''}}" aria-current="page" href="{{Route('article.index')}}">Articoli</a>
          <a class="nav-link {{Route::is('author.create') ? 'active' : ''}}" aria-current="page" href="{{Route('author.create')}}">Registra un Autore</a>
          <a class="nav-link {{Route::is('author.index') ? 'active' : ''}}" aria-current="page" href="{{Route('author.index')}}">Autori</a>
          @auth
            <a class="nav-link active">Ciao {{Auth::user()->name}} !</a>
            <a class="nav-link {{Route::is('profile') ? 'active' : ''}}" aria-current="page" href="{{Route('profile')}}">Profilo</a>
            <a class="nav-link" aria-current="page" href="#" onclick="event.preventDefault(); document.querySelector('#form-logout').submit();">Logout</a>
            <form action="{{route('logout')}}" id="form-logout" method="POST" class="d-none">
              @csrf
            </form>
          @else
            <a class="nav-link {{Route::is('register') ? 'active' : ''}}" href="{{route('register')}}">Iscriviti</a>
            <a class="nav-link {{Route::is('login') ? 'active' : ''}}" href="{{route('login')}}">Login</a>
          @endauth
        </div>
      </div>
    </div>
  </nav>
<x-layout>
    {{-- DEFINISCO IL TITOLO --}}
    <x-slot name="title">Home</x-slot>
    {{-- messaggi per l'utente --}}
    {{-- invio messaggi --}}
    @if(session('success'))
        <div class="alert alert-success m-1">
            <p class="m-1">{{session('success')}}</p>
        </div>
    @endif
    @if(session('fail'))
        <div class="alert alert-danger m-1">
            <p class="m-1">{{session('fail')}}</p>
        </div>
    @endif
    {{-- pubblicazione articoli --}}
    @if(session('articleCreated'))
        <div class="alert alert-success m-1">
            <p class="m-1">{{session('articleCreated')}}</p>
        </div>
    @endif
    @if(session('articleFail'))
        <div class="alert alert-danger m-1">
            <p class="m-1">{{session('articleFail')}}</p>
        </div>
    @endif
    {{-- pubblicazione autori --}}
    @if(session('authorSuccess'))
        <div class="alert alert-success m-1">
            <p class="m-1">{{session('authorSuccess')}}</p>
        </div>
    @endif
    {{-- header --}}
    <div class="container m-5">
        <p class="fw-bold container-fluid">Benvenuto su</p>
        <h1 class="fw-bold container-fluid">MyDailyRoutine</h1>
        <p class="fw-bold container-fluid">Condivi il tuo stile di vita e scoprine di nuovi da tutto il mondo !</p>
    </div>
    {{-- header sezione articoli --}}
    <div class="container my-5">
        <h1 class="fw-bold text-center">Dai un'occhiata a questi Articoli !</h1>
    </div>
    {{-- sezione articoli --}}
    <div class="container my-5">
        <h6 class="fw-bold text-center">Articoli Recenti</h6>
        <div class="row justify-content-center">
            @if (count($articles) > 0)
                @foreach($articles as $article)
                    <div class="col-12 col-md-4">
                        <div class="card mb-3">
                            <img src="{{Storage::url($article->cover)}}" class="img card-img-bottom" alt="copertina">
                            <div class="card-body">
                            <h5 class="card-title">{{$article['title']}}</h5>
                            <h6 class="card-subtitle mb-2 text-muted">{{$article->user->name}}</h6>
                            <a href="{{route('article.detail', ['id' => $article['id']])}}" class="btn btn-info text-light">Leggi articolo</a>
                            </div>
                        </div>
                    </div>
                @endforeach
            @else
            <div class="col-12 col-md-5">
                <h2>Ancora nessun articolo... <a href="{{route('article.create')}}" class="btn btn-success text-light">Scrivine uno</a></h2>
            </div>
            @endif
        </div>
    </div>
    {{-- sezione autori --}}
    <div class="container my-5">
        <h6 class="fw-bold text-center">Alcuni dei nostri Autori</h6>
        <div class="row justify-content-center">
            @if (count($authors) > 0)
                @foreach($authors as $author)
                    <div class="col-12 col-md-4">
                        <div class="card mb-3">
                            <img src="{{Storage::url($author->photo)}}" class="img card-img-bottom" alt="copertina">
                            <div class="card-body">
                            <h5 class="card-title">{{$author['name']}}</h5>
                            <h6 class="card-subtitle mb-2 text-muted">{{$author['email']}}</h6>
                            <a href="{{route('author.show',compact('author'))}}" class="btn btn-info text-light">Pagina Autore</a>
                            </div>
                        </div>
                    </div>
                @endforeach
            @else
            <div class="col-12 col-md-5">
                <h2>Ancora nessun autore... <a href="{{route('author.create')}}" class="btn btn-success text-light">Aggiungine uno</a></h2>
            </div>
            @endif
        </div>
    </div>
</x-layout>
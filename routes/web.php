<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthorController;
use App\Http\Controllers\PublicController;
use App\Http\Controllers\ArticleController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//PUBLICROUTES
Route::get('/',[PublicController::class,'homePage'])->name('homePage');
Route::post('/submit', [PublicController::class, 'submit'])->name('submit');
Route::get('/user/profile',[PublicController::class,'profile'])->name('profile');
//ARTICLEROUTES
Route::get('/article/create',[ArticleController::class,'create'])->name('article.create');
Route::post('/article/store',[ArticleController::class,'store'])->name('article.store');
Route::get('/article/index',[ArticleController::class,'index'])->name('article.index');
Route::get('/article/{id}/detail',[ArticleController::class, 'detail'])->name('article.detail');
Route::get('/article/{article}/edit',[ArticleController::class, 'edit'])->name('article.edit');
Route::put('/article/{article}/update',[ArticleController::class, 'update'])->name('article.update');
Route::delete('/article/{article}/destroy',[ArticleController::class, 'destroy'])->name('article.destroy');
//AUTHORROUTES
Route::get('/author/create',[AuthorController::class,'create'])->name('author.create');
Route::post('/author/store',[AuthorController::class,'store'])->name('author.store');
Route::get('/author/index',[AuthorController::class,'index'])->name('author.index');
Route::get('/author/{author}/show',[AuthorController::class, 'show'])->name('author.show');
Route::get('/author/{author}/edit',[AuthorController::class, 'edit'])->name('author.edit');
Route::put('/author/{author}/update',[AuthorController::class, 'update'])->name('author.update');
Route::delete('/author/{author}/destroy',[AuthorController::class, 'destroy'])->name('author.destroy');